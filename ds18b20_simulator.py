# This is just a simulator to check the whole code. It does nohing else than mocking a ds18b20 outputfile /w1_slave in the local /test folder
import random
from io import open
from time import sleep
from os import makedirs, path
from sys import argv


PATH= "test/sensorID/"
LOWERBOUND_TEMP = -20000
UPPERBOUND_TEMP = 40000
LOWERBOUND_CHANGE_TEMP = -1000
UPPERBOUND_CHANGE_TEMP = 1000
REFRESH_RATE = 0.5
CRC = "YES"
lastTemp = 0
DEBUG = False

if argv[1] and (argv[1]=="DEBUG"):
    DEBUG = True

class ds18b20Simulator:

    def __init__(self):
        directories = path.dirname(PATH)
        self.ensurePathExists(directories)
        self.lastTemp = self.defineStartingTemperature()

    def ensurePathExists(self,directory):
        if not path.exists(directory):
            makedirs(directory)

    def defineStartingTemperature(self):
        random.seed()
        temperature = random.randint(LOWERBOUND_TEMP, UPPERBOUND_TEMP)
        return temperature

    def writeSensorFile(self):
        sensoroutput = open(PATH + 'w1_slave', 'wb+')
        sensoroutput.write('f6 01 4b 46 7f ff 0a 10 eb : crc=%s \nf6 01 4b 46 7f ff 0a 10 eb t=%i' % (CRC ,self.lastTemp))
        sensoroutput.close()
        if(DEBUG): print(self.lastTemp)

    def generatureNewTemperature(self):
        random.seed()
        temperature_change = random.randint(LOWERBOUND_CHANGE_TEMP, UPPERBOUND_CHANGE_TEMP)
        self.lastTemp = self.lastTemp + temperature_change

if __name__ == '__main__':
    simulator = ds18b20Simulator()
    while True:
        sleep(REFRESH_RATE)
        simulator.generatureNewTemperature()
        simulator.writeSensorFile()
